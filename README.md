## Overview
A DIY version of the popular Linktree used on Instagram.

To tweak the project, you can modify `index.html` at the root and files in the `app` directory.

You can view the end product [here](https://paperduck.gitlab.io/linktree/).
## Commands

For building the application,
run the command `gulp` on the terminal.

To generate a build for GitLab pages, use `npm run build`.
