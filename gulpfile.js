const gulp = require('gulp');
const sass = require('gulp-sass');
const uglifyCSS = require('gulp-uglifycss');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

// Build Stylesheet
async function stylesheet() {
  gulp.src('app/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(uglifyCSS())
    .pipe(gulp.dest('./dist'));
}

// Build Javascript
async function javascript() {
  gulp.src('app/js/*.js')
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
}

// Watch tasks
function watch() {
  gulp.watch('app/scss/*.scss', gulp.series(stylesheet, javascript));
};

// Default task
exports.default = gulp.series(stylesheet, javascript, watch);